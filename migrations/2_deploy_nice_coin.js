const NiceCoin = artifacts.require('./NiceCoin.sol')

module.exports = (deployer) => {
    const tokenName = "NiceCoin"
    const symbol = "NIC"
    const decimal = 18
    deployer.deploy(NiceCoin, tokenName, symbol, decimal)
}