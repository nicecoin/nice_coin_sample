pragma solidity ^0.5.0;
//import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
//import "openzeppelin-solidity/contracts/token/StandardToken.sol";
import "node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

contract NiceCoin is ERC20 {
    string public name = ""; 
    string public symbol = "";
    uint public decimals = 18;
    uint public INITIAL_SUPPLY = 10000 * (10 ** decimals);
    
    //    event Replenish(bool indexed _isReplenish, address indexed _sender,uint _value);
    event CTransfer( 
    uint indexed _date, 
    address indexed _from,
    address indexed _to,
    uint256 _value);

    uint constant private distCoin = 10; // distribution coin value
    
    address _account = msg.sender;

    constructor(string memory _tokenName, string memory _symbol, uint _decimals ) public {
        name = _tokenName;
        symbol = _symbol;
        decimals = _decimals;
        _mint(_account,INITIAL_SUPPLY);
    }

    //
    // override _transfer function
    // original event 
    //
    function _transfer(address from, address to, uint256 value) internal{
        super._transfer(from,to,value);
        emit CTransfer(block.timestamp,from,to,value);
    }
    //
    // coin replenish function
    //
    function replenish() public{
        require(balanceOf(_account)-distCoin >= 0);
        require(_account!=msg.sender);
        _transfer(_account,msg.sender, distCoin);
    }
    //
}
